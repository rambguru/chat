import React from "react";
import { markdownifyToString, tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { YesNo } from "tripetto-block-yes-no/runner";
import { IChatRenderProps, IChatRendering } from "@interfaces/block";
import { YesNoFabric } from "tripetto-runner-fabric/components/yes-no";
import { ParagraphMessage } from "@ui/message/paragraph";
import { ParagraphImage } from "@ui/message/paragraph/image";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-yes-no",
    alias: "yes-no",
})
export class YesNoBlock extends YesNo implements IChatRendering {
    private getLabel(props: IChatRenderProps, answer: string): string | undefined {
        switch (answer) {
            case "yes":
                return markdownifyToString(this.props.altYes || "", this.context) || props.l10n.pgettext("runner#6|🔷 Yes/No", "Yes");
            case "no":
                return markdownifyToString(this.props.altNo || "", this.context) || props.l10n.pgettext("runner#6|🔷 Yes/No", "No");
        }

        return undefined;
    }

    question(props: IChatRenderProps): React.ReactNode {
        return (
            ((props.name || props.description || this.props.imageURL) && (
                <ParagraphMessage>
                    {this.props.imageURL && this.props.imageAboveText && (
                        <ParagraphImage src={props.markdownifyToURL(this.props.imageURL, this.context)} width={this.props.imageWidth} />
                    )}
                    {props.name}
                    {props.description}
                    {this.props.imageURL && !this.props.imageAboveText && (
                        <ParagraphImage src={props.markdownifyToURL(this.props.imageURL, this.context)} width={this.props.imageWidth} />
                    )}
                </ParagraphMessage>
            )) ||
            undefined
        );
    }

    answer(props: IChatRenderProps): React.ReactNode {
        return (this.answerSlot.reference && this.getLabel(props, this.answerSlot.reference)) || undefined;
    }

    input(props: IChatRenderProps, done?: () => void, cancel?: () => void): React.ReactNode {
        return (
            <>
                <YesNoFabric
                    styles={props.styles.yesNo}
                    value={this.answerSlot}
                    yes={{
                        label: this.getLabel(props, "yes") || "",
                    }}
                    no={{
                        label: this.getLabel(props, "no") || "",
                    }}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={true}
                    onAutoFocus={props.autoFocus()}
                    onFocus={props.focus()}
                    onBlur={props.blur()}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
            </>
        );
    }
}
