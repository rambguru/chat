import { mountNamespace, unmountNamespace } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import "./polyfills";

mountNamespace(namespace);

export { ChatRunner } from "./chat";
export { IChatProps, TChatDisplay, TChatPause } from "@interfaces/props";
export { IChatSnapshot } from "@interfaces/snapshot";
export { IChatRunner } from "@interfaces/runner";
export { IChatStyles } from "@interfaces/styles";
export { IChatController } from "@hooks/controller";
export { IChatRendering as IBlockRendering, IChatRenderProps as IBlockProps } from "@interfaces/block";
export { run } from "./run";
export { namespace } from "./namespace";

unmountNamespace();
