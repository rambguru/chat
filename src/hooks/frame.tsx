import React, { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";
import { useFontLoader } from "tripetto-runner-fabric/fontloader";

export const Frame = (props: {
    readonly children?: ((document: Document, fontFamily: string) => ReactNode) | ReactNode;
    readonly frameRef?: MutableRefObject<HTMLIFrameElement>;
    readonly resizeRef?: MutableRefObject<(() => void) | undefined>;
    readonly title?: string;
    readonly style?: CSSProperties;
    readonly className?: string;
    readonly font?: string;
    readonly onTouch?: () => void;
}) => {
    const ref = props.frameRef || (useRef<HTMLIFrameElement>() as MutableRefObject<HTMLIFrameElement>);
    const [doc, setDoc] = useState<Document | undefined>();
    const [isFontLoading, fontFamily] = useFontLoader(props.font || "sans-serif", ref, "sans-serif");

    useEffect(() => {
        const updateDoc = () => {
            if (!doc) {
                const contentDocument = ref.current.contentDocument;

                ref.current.contentWindow?.addEventListener("resize", () => {
                    if (props.resizeRef && props.resizeRef.current) {
                        props.resizeRef.current();
                    }
                });

                if (props.onTouch) {
                    const onTouch = props.onTouch;

                    ref.current.contentWindow?.addEventListener("mousedown", () => onTouch());
                    ref.current.contentWindow?.addEventListener("touchstart", () => onTouch());
                    ref.current.contentWindow?.addEventListener("wheel", () => onTouch());
                }

                if (contentDocument) {
                    setDoc(contentDocument);
                }
            }
        };

        if (!doc) {
            const contentDocument = ref.current.contentDocument;

            if (contentDocument && contentDocument.readyState === "complete") {
                return updateDoc();
            } else {
                ref.current.addEventListener("load", updateDoc);
            }
        }

        return () => {
            ref.current.removeEventListener("load", updateDoc);
        };
    }, []);

    return (
        <iframe
            ref={ref}
            title={props.title}
            style={{
                ...props.style,
                opacity: (isFontLoading && "0") || undefined,
                boxShadow: (isFontLoading && "none") || undefined,
            }}
            className={props.className}
        >
            {doc &&
                !isFontLoading &&
                createPortal(typeof props.children === "function" ? props.children(doc, fontFamily) : props.children, doc.body)}
        </iframe>
    );
};
