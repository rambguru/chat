import React from "react";
import styled from "styled-components";
import { SIZE } from "../../const";

const getYouTubeId = (url: string) => {
    const videoID = url.match(/youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/);

    return (videoID && videoID.length === 2 && videoID[1]) || "";
};

const getVimeoId = (url: string) => {
    const videoID = url.match(/\/\/(?:www\.)?vimeo\.com\/(?:channels\/staffpicks\/)?([-\w]+)/i);

    return (videoID && videoID.length === 2 && videoID[1]) || "";
};

export const ParagraphVideoElement = styled.div`
    margin: ${16 / SIZE}em;
    padding: 0;
    border: none;
    width: calc(100% - ${32 / SIZE}em);
    height: calc((75vw - ${32 / SIZE}em) * 0.5625);

    &:only-child {
        margin: 0;
        width: 100%;
        height: calc(75vw * 0.5625);
    }

    > iframe {
        width: 100%;
        height: 100%;
        border: none;
    }
`;

export const ParagraphVideo = (props: { readonly src: string }) => {
    const youTubeId = getYouTubeId(props.src);
    const vimeoId = getVimeoId(props.src);

    return youTubeId || vimeoId ? (
        <ParagraphVideoElement>
            {youTubeId && (
                <iframe
                    src={"https://www.youtube-nocookie.com/embed/" + youTubeId}
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                />
            )}
            {vimeoId && <iframe src={"https://player.vimeo.com/video/" + vimeoId} allow="autoplay; fullscreen" />}
        </ParagraphVideoElement>
    ) : (
        <></>
    );
};
