const webpack = require("webpack");
const webpackTerser = require("terser-webpack-plugin");
const webpackShell = require("webpack-shell-plugin-next");
const webpackCopy = require("copy-webpack-plugin");
const path = require("path");
const banner = require("./tasks/banner/banner.js");
const package = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const config = (dest, target) => {
    const externals =
        dest !== "test"
            ? {
                  tripetto: target === "umd" ? "Tripetto" : "commonjs tripetto",
                  "tripetto-runner-foundation": target === "umd" ? "TripettoRunner" : "commonjs tripetto-runner-foundation",
                  ...(target !== "umd"
                      ? {
                            react: "commonjs react",
                            "react-dom": "commonjs react-dom",
                            "styled-components": "commonjs styled-components",
                        }
                      : {}),
              }
            : {};
    let entry;
    let output;

    switch (dest) {
        case "runner":
            entry = "./src/index.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "runner", (target !== "umd" && target) || ""),
                library: "TripettoChat",
                libraryTarget: target === "umd" ? "umd" : "commonjs2",
                umdNamedDefine: true,
            };
            break;
        case "builder":
            entry = "./src/builder/index.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "builder", (target !== "umd" && target) || ""),
                library: "TripettoChatBuilder",
                libraryTarget: target === "umd" ? "umd" : "commonjs2",
                umdNamedDefine: true,
            };
            break;
        default:
            entry = "./src/tests/app/app.tsx";
            output = {
                filename: "bundle.js",
                path: path.resolve(__dirname, "src/tests/app/static"),
            };
            break;
    }

    return {
        entry,
        output,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    options: {
                        compilerOptions: {
                            noEmit: false,
                            target: target === "es6" ? "ES6" : "ES5",
                            module: target === "es6" ? "es6" : "commonjs",
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js"],
            alias: {
                "@namespace": path.resolve(__dirname, "./src/namespace"),
                "@blocks": path.resolve(__dirname, "./src/blocks"),
                "@helpers": path.resolve(__dirname, "./src/helpers"),
                "@hooks": path.resolve(__dirname, "./src/hooks"),
                "@interfaces": path.resolve(__dirname, "./src/interfaces"),
                "@l10n": path.resolve(__dirname, "./builder/l10n"),
                "@styles": path.resolve(__dirname, "./src/styles"),
                "@ui": path.resolve(__dirname, "./src/ui"),
            },
        },
        externals,
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        output: {
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
                new webpack.BannerPlugin(banner),
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                PACKAGE_NAME: JSON.stringify(package.name),
                PACKAGE_TITLE: JSON.stringify(package.title),
                PACKAGE_VERSION: JSON.stringify(package.version),
            }),
            ...(dest === "runner" && target === "umd"
                ? [
                      new webpackCopy({
                          patterns: [{ from: "translations/template.pot", to: "translations/" }],
                      }),
                  ]
                : []),
            ...(dest !== "test" && target === "umd"
                ? [
                      new analyzer({
                          analyzerMode: "static",
                          reportFilename: `../reports/bundle-${dest}-${target}.html`,
                          openAnalyzer: false,
                      }),
                  ]
                : []),
            ...(dest === "test"
                ? [
                      new webpackShell({
                          onBuildStart: {
                              scripts: ["npm run make:l10n"],
                              blocking: true,
                              parallel: false,
                          },
                      }),
                      new webpackCopy({
                          patterns: [{ from: "node_modules/tripetto/fonts/", to: "fonts/" }],
                      }),
                  ]
                : []),
        ],
        devServer: {
            contentBase: path.join(__dirname, "src/tests/app/static"),
            port: 9000,
            host: "0.0.0.0",
        },
    };
};

module.exports = production
    ? [
          config("builder", "umd"),
          config("builder", "es5"),
          config("builder", "es6"),
          config("runner", "umd"),
          config("runner", "es5"),
          config("runner", "es6"),
          config("test"),
      ]
    : [config("test")];
