const fs = require("fs");
const tripetto = require("tripetto");
const package = require("../../package.json");

const contractFile = "./builder/styles/index.js";
let contractData = fs.readFileSync(contractFile, "utf-8");

contractData = tripetto.Str.replace(contractData, "PACKAGE_NAME", `"${package.name}"`);
contractData = tripetto.Str.replace(contractData, "PACKAGE_VERSION", `"${package.version}"`);

fs.writeFileSync(contractFile, contractData, "utf8");
