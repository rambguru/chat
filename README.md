![Tripetto](https://unpkg.com/tripetto/assets/banner.svg)

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Chat runner
[![Version](https://img.shields.io/npm/v/tripetto-runner-chat.svg)](https://www.npmjs.com/package/tripetto-runner-chat)
[![License](https://img.shields.io/npm/l/tripetto-runner-chat.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-runner-chat.svg)](https://www.npmjs.com/package/tripetto-runner-chat)
[![Pipeline status](https://gitlab.com/tripetto/runners/chat/badges/master/pipeline.svg)](https://gitlab.com/tripetto/runners/chat/commits/master)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

# Purpose
This package is a chat UI for running Tripetto forms and surveys. In this UI the questions and elements (so called blocks) are displayed like a chat message. That allows you to build highly conversational interactions with your users.

[![Try the demo](https://unpkg.com/tripetto/assets/button-demo.svg)](https://tripetto.gitlab.io/runners/chat/)

# Get started
There a multiple options how you can use this runner. From plain old HTML to [React](https://reactjs.org/) or using imports.

## Option A: Embed in HTML using CDN
```html
<script src="https://unpkg.com/tripetto-runner-foundation"></script>
<script src="https://unpkg.com/tripetto-runner-chat"></script>
<script>
TripettoChat.run({
    definition: /** Supply a form definition here. */,
    onSubmit: function(instance) {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(TripettoRunner.Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(TripettoRunner.Export.CSV(instance));
    }
});
</script>
```
[![Try demo on CodePen](https://unpkg.com/tripetto/assets/button-codepen.svg)](https://codepen.io/tripetto/pen/jOWEMRK)

## Option B: Using React
1. Install the required packages from npm:
```bash
$ npm install tripetto-runner-foundation tripetto-runner-chat tripetto-runner-react-hook react react-dom
```
2. Use the React component:
```typescript
import React from "react";
import ReactDOM from "react-dom";
import { ChatRunner } from "tripetto-runner-chat";
import { Export } from "tripetto-runner-foundation";

ReactDOM.render(
  <ChatRunner
    definition={/** Supply a form definition here. */}
    onSubmit={instance => {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(Export.CSV(instance));
    }}
    />,
  document.getElementById("your-element")
);
```

## Option C: Import from npm
1. Install the required packages from npm:
```bash
$ npm install tripetto-runner-foundation tripetto-runner-chat tripetto-runner-react-hook react react-dom
```
2. Import the runner:
```typescript
import { run } from "tripetto-runner-chat";
import { Export } from "tripetto-runner-foundation";

run({
    definition: /** Supply a form definition here. */,
    onSubmit: instance => {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(Export.CSV(instance));
    }
});
```

# Documentation
The complete Tripetto documentation can be found at [docs.tripetto.com](https://docs.tripetto.com).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/runners/chat/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
